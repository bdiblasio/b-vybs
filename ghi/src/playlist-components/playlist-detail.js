import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useToken } from '../user-components/token';
import { useNavigate } from "react-router-dom";
import AddTrackPage from "./add-track";
import "./playlist-detail.css"

function PlaylistDetail () {
    const {playlist_id} = useParams();
    const {token} = useToken();
    const [playlist, setPlaylist] = useState({});
    const [tracks, setTracks] = useState([])

    const fetchData = async () => {
        const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/playlist/${playlist_id}`
        const fetchConfig = {
            method: "get",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            setPlaylist(data)
        }

        const trackUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/playlist/${playlist_id}/tracks`
        const fetchConfig1 = {
            method: "get",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
        }
        const response1 = await fetch(trackUrl, fetchConfig1)
        if (response1.ok) {
            const data1 = await response1.json()
            setTracks(data1)
        }

    }
    useEffect(() => {
        if (token) {
            fetchData()
        }
    }, [playlist_id, token])
    if (!playlist) {
        return (<>Loading...</>)
    }
    return (
        <div>
            <div>
                <h1>{playlist.name}</h1>
                <p>{playlist.description}</p>
                <h2>{playlist.mood}</h2>
            </div>
            <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Artist</th>
              <th>Song Length</th>
              <th>Album</th>
              <th>Album Cover</th>

            </tr>
          </thead>
          <tbody>
            {tracks.map((track, id) => {
              return (
                <tr key={id}>
                    <td>{track.name} </td>
                    <td>{ track.artist }</td>
                    <td>{track.song_length}</td>
                    <td>{track.album}</td>
                    <td><img className="cover-art" src={track.album_cover}></img></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>

    );
}
export default PlaylistDetail
