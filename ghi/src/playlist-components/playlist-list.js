import React, { useState, useEffect } from "react";
import { useToken } from "../user-components/token";
import { Link } from "react-router-dom";
import DeleteButton from "./delete";
import "./playlist-list.css"


function PlaylistListPage() {
 const [playlists, setPlaylists] = useState([]);
 const { token } = useToken();
 const fetchData = async () => {
   const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token/playlists`;


   const fetchConfig = {
     method: "get",
     headers: {
       Authorization: `Bearer ${token}`,
       "Content-Type": "application/json",
     },
   };
   const response = await fetch(url, fetchConfig);
   if (response.ok) {
     const data = await response.json();
     setPlaylists(data);
   }
 };
 useEffect(() => {
  if (token) {
   fetchData();
  }
 }, [token]);


   return (
       <>
       <div id='playlist_list_component'>
           {/* <div id='playlist_list_componentt' className="px-4 py-5 my-5 mt-0 text-center bg-info">
               <h1 className="display-5 fw-bold">Good Vibes!</h1>
               <div className="col-lg-6 mx-auto">
               <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
               </div>
               </div>
           </div> */}
           <div className="container">
               <h2>Current Playlists</h2>
               <table className="table table-striped">
               <thead>
                   <tr>
                   <th>Name</th>
                   <th>Description</th>
                   <th>Mood</th>
                   <th>ID</th>
                   </tr>
               </thead>
               <tbody>
                   {playlists.map((playlist, id) => {
                   return (
                     <tr key={id}>
                       <td>
                         <Link to={`/playlist/${playlist.id}`}>
                           {playlist.name}
                         </Link>
                       </td>
                       <td>{playlist.description}</td>
                       <td>{playlist.mood}</td>
                       <td>{playlist.id}</td>
                       <td>
                         <Link to={`/playlist/update/${playlist.id}`}>
                           {" "}
                           Edit{" "}
                         </Link>
                       </td>
                       <td>
                         <DeleteButton
                           playlist_id={playlist.id}
                           fetchData={fetchData}
                         />
                       </td>
                       <td>
                         {" "}
                         <Link to={`/search/${playlist.id}`}> Add a Song </Link>{" "}
                       </td>
                     </tr>
                   );
                   })}
               </tbody>
               </table>
           </div>
       </div>
       </>
 );
}


export default PlaylistListPage;
