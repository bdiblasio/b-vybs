import { NavLink } from "react-router-dom";
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from "react-bootstrap/DropdownButton";
import "./Nav.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { useToken} from "./user-components/token";
import { useUser } from "./user-components/token";

function DropdownNav() {
    const { token } = useToken()

    return (
      <Navbar bg="light">
        <Container>
          <Navbar.Brand href="/">HOME</Navbar.Brand>
          {/* <Navbar.Toggle aria-controls="basic-navbar-nav" /> */}
          {/* <Navbar.Collapse
              className="justify-content-end"
              id="basic-navbar-nav"
            > */}
          <Nav className="me-auto">
            {token ? (
              <NavDropdown title="WELCOME" id="basic-nav-dropdown">
                <NavDropdown.Item href="/logout">Logout </NavDropdown.Item>
                <NavDropdown.Item href="/account">User Detail </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <NavDropdown title="USER" id="basic-nav-dropdown">
                <NavDropdown.Item href="/login">Login </NavDropdown.Item>
                <NavDropdown.Item href="/signup">Sign Up </NavDropdown.Item>
              </NavDropdown>
            )}
            { token ? (
              <NavDropdown title="PLAYLISTS" id="basic-nav-dropdown">
                <NavDropdown.Item href="/token/playlists">
                  See All{" "}
                </NavDropdown.Item>
                <NavDropdown.Item href="/playlist/new">Create </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <NavDropdown title="PLAYLISTS" id="basic-nav-dropdown">
                <NavDropdown.Item href="/login">Please sign in to view</NavDropdown.Item>
              </NavDropdown>
            )}
            {/* <NavDropdown title="USER" id="basic-nav-dropdown">
              <NavDropdown.Item href="/login">Login </NavDropdown.Item>
              <NavDropdown.Item href="/logout">Logout </NavDropdown.Item>
              <NavDropdown.Item href="/signup">Sign Up </NavDropdown.Item>
              <NavDropdown.Item href="/account">User Detail </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="PLAYLISTS" id="basic-nav-dropdown">
              <NavDropdown.Item href="/token/playlists">
                See All{" "}
              </NavDropdown.Item>
              <NavDropdown.Item href="/playlist/new">Create </NavDropdown.Item>
                  {/* <NavDropdown.Item href="/liked-albums">Liked Albums </NavDropdown.Item> */}
            {/* </NavDropdown> */}
          </Nav>
          {/* </Navbar.Collapse> */}
        </Container>
      </Navbar>
    );
}

export default DropdownNav;

// function SimpleNav() {
//   return (
//     <nav class="navbar">
//       <ul className="navbar-root">
//         <li className="nav-item">
//           <NavLink
//             className="nav-link "
//             aria-current="page"
//             to="/"
//             id="navbarDarkDropdownMenuLink"
//             role="button"
//             data-bs-toggle=""
//             aria-expanded="false"
//           >
//             HOME
//           </NavLink>
//         </li>
//       </ul>

//       <ul className="navbar-root">
//         <li className="nav-item">
//           <NavLink
//             className="nav-link "
//             aria-current="page"
//             to="/appointments/"
//             id="navbarDarkDropdownMenuLink"
//             role="button"
//             data-bs-toggle="dropdown"
//             aria-expanded="false"
//           >
//             USER
//           </NavLink>
//           <ul
//             class="dropdown-menu dropdown-menu-dark"
//             aria-labelledby="navbarDarkDropdownMenuLink"
//           >
//             <li>
//               <NavLink
//                 className="nav-link"
//                 aria-current="page"
//                 to="/appointments/"
//               >
//                 login
//               </NavLink>
//             </li>
//             <li>
//               <NavLink
//                 className="nav-link"
//                 aria-current="page"
//                 to="/appointments/new/"
//               >
//                 logout
//               </NavLink>
//             </li>
//           </ul>
//         </li>
//       </ul>

//       <ul className="navbar-root">
//         <li className="nav-item">
//           <NavLink
//             className="nav-link "
//             aria-current="page"
//             to="/appointments/"
//             id="navbarDarkDropdownMenuLink"
//             role="button"
//             data-bs-toggle="dropdown"
//             aria-expanded="false"
//           >
//             PLAYLISTS
//           </NavLink>
//           <ul
//             class="dropdown-menu dropdown-menu-dark"
//             aria-labelledby="navbarDarkDropdownMenuLink"
//           >
//             <li>
//               <NavLink className="nav-link" aria-current="page" to="/history/">
//                 create new
//               </NavLink>
//             </li>
//             <li>
//               <NavLink
//                 className="nav-link"
//                 aria-current="page"
//                 to="technicians/"
//               >
//                 list all
//               </NavLink>
//             </li>
//           </ul>
//         </li>
//       </ul>

//     </nav>
//   );
// }

// export default SimpleNav;
