
import React, { useState, useEffect } from 'react';
import { useToken } from '../user-components/token';
import { useAuthContext } from '../user-components/token';
import './user-detail.css'

function UserDetail() {
    const [account, setAccount] = useState([])
    const { token } = useToken();
    const fetchData = async () => {

        const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`;
        const fetchConfig = {
            method: "get",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json",
            },
            credentials: "include"
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            setAccount(data.account)


        }
    }
    useEffect(() => {
        fetchData()
    }, [token])

    // return (

    //     <table className="table table-striped">
    //         <thead className="thead-dark">
    //             <tr>
    //                 <th>Avatar</th>
    //                 <th>Username</th>
    //                 <th>Full Name</th>
    //                 <th>Email</th>
    //             </tr>
    //         </thead>
    //         <tbody>
    //             <tr>
    //                 <td><img src={account.avatar} className="img-fluid"/></td>
    //                 <td>{account.username}</td>
    //                 <td>{account.full_name}</td>
    //                 <td>{account.email}</td>
    //             </tr>
    //         </tbody>
    //     </table>
    //     )}

//           return (
//     <div className="card mb-3">
//       <div className="card-header">
//         <h5 className="card-title">User Details</h5>
//       </div>
//       <div className="card-body">
//         <div className="row">
//           <div className="col-4">
//             <img src={account.avatar} className="img-fluid rounded user-avatar" alt="User Avatar" />
//           </div>
//           <div className="col-8">
//             <div className="row">
//               <div className="col">
//                 <h6>Username:</h6>
//                 <p>{account.username}</p>
//               </div>
//               <div className="col">
//                 <h6>Full Name:</h6>
//                 <p>{account.full_name}</p>
//               </div>
//             </div>
//             <div className="row">
//               <div className="col">
//                 <h6>Email:</h6>
//                 <p>{account.email}</p>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   )
// }
    return (
        <div className="user-detail-page">
            <div className="user-detail-container">
                <div className="user-detail-card-container">
                    <div className="card card-purple">
                        <img src={account.avatar} className="card-img-top user-avatar test" alt="User Avatar"/>
                        <div className="avatar-circle"></div>
                        <div className="card-body card-purple">
                            <div className="pink-box">
                                <h5 className="card-title">Username</h5>
                                <p className="card-text">{account.username}</p>
                            </div>
                            <div className="pink-box">
                                <h5 className="card-title">Name</h5>
                                <p className="card-text">{account.full_name}</p>
                            </div>
                            <div className="pink-box">
                                <h5 className="card-title">Email</h5>
                                <p className="card-text">{account.email}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
    }
export default UserDetail;
