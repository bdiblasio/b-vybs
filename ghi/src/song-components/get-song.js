import React, { useState, useEffect } from "react";
import { useToken } from "../user-components/token";
import { useAuthContext } from "../user-components/token";

function GetSong() {
  const [song, setSong] = useState([]);
  const { token } = useToken();
  const fetchData = async () => {
    const url = `https://api.spotify.com/v1/tracks`;
    const fetchConfig = {
      method: "get",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      credentials: "include",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      setAccount(data.account);
    }
  };
  useEffect(() => {
    fetchData();
  }, [token]);

  return (
    <table className="table table-striped">
      <thead className="thead-dark">
        <tr>
          <th>Avatar</th>
          <th>Username</th>
          <th>Full Name</th>
          <th>Email</th>
          <th>ID</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <img src={account.avatar} className="img-fluid" />
          </td>
          <td>{account.username}</td>
          <td>{account.full_name}</td>
          <td>{account.email}</td>
          <td>{account.id}</td>
        </tr>
      </tbody>
    </table>
  );
}

export default GetSong;
