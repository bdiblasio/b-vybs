from fastapi.testclient import TestClient
from main import app
from db import PlaylistQueries
from authenticator import authenticator

client = TestClient(app)

def fake_get_current_account_data():
    return {
        'id' : '111',
        'name' : 'Sad Songs',
        'description' : 'Hi',
        'mood' : 'OK'
    }

class FakePlaylistQueries:
    def create_playlist(self, playlist_in):
        playlist_dict = playlist_in.dict()
        playlist_dict['id'] = 1
        playlist_dict['name'] =


    def get_all_playlists(self):
        return []

def get_all_playlists ():
    #Arrange
    app.dependency_overrides[PlaylistQueries] = FakePlaylistQueries
    app.dependency_overrides[authenticator.get_current_account_data] = fake_get_current_account_data

    #Act

    response = client.get('/token/playlists')
    data = response.json()

    #Assert (needs boolean)
    assert data['playlists'] == []
    assert response.status_code == 200



# @router.get("/token/playlists", tags=["playlists"])
# def get_all_playlists(
#     repo: PlaylistQueries = Depends(),
#     account_data: dict = Depends(authenticator.get_current_account_data),
# ):
#     return repo.get_all_playlists(account_id=account_data["id"])


# def get_all_playlists(self, account_id: str) -> PlaylistOut:
#         with pool.connection() as conn:
#             with conn.cursor() as cur:
#                 cur.execute(
#                     """
#                     SELECT id
#                          , name
#                          , description
#                          , mood
#                     FROM playlist
#                     WHERE account_id = %s;
#                     """,
#                     [account_id],
#                 )
#                 results = []
#                 for row in cur.fetchall():
#                     playlist = {}
#                     print(row)
#                     print(cur.description)
#                     for i, column in enumerate(cur.description):
#                         playlist[column.name] = row[i]
#                     results.append(playlist)
#                 return results
