from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from models.track_playlist import Track_playlistIn, Track_playlistOut
from queries.track_playlist import Track_playlistQueries
from authenticator import authenticator

router = APIRouter()


@router.post("/track_playlist/{track_id}/{playlist_id}/",
    response_model=Track_playlistOut, tags=["Track Playlist"])
def create_track_playlist(
    track_id: int,
    playlist_id: int,
    response: Response,
    repo: Track_playlistQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    # if track_playlist.id is not None:
    return repo.create({"track_id": track_id, "playlist_id": playlist_id})


@router.get('/playlist/{playlist_id}/tracks', tags=["Track Playlist"])
def get_playlist_tracks(
    playlist_id: int,
    repo: Track_playlistQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.get_tracks_from_playlist(playlist_id)
