from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import accounts, playlists, songs, track_playlist
import os
from authenticator import authenticator

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(accounts.router)
app.include_router(authenticator.router)
app.include_router(playlists.router)
app.include_router(songs.router)
app.include_router(track_playlist.router)
