from models.track_playlist import Track_playlistIn, Track_playlistOut
from queries.pool import pool
from models.playlists import PlaylistIn, PlaylistOut


class Track_playlistQueries:
    def create(
        self,
        track_playlist: Track_playlistIn,
    ) -> Track_playlistOut:
        print("+++++++", track_playlist)
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                        INSERT INTO track_playlist (track_id, playlist_id)
                        VALUES (%s, %s)
                        RETURNING id;
                        """,
                    [track_playlist["track_id"], track_playlist["playlist_id"]],
                )
                id = result.fetchone()[0]
                print(id)
                return Track_playlistOut(
                    id=id,
                    track_id=track_playlist["track_id"],
                    playlist_id=track_playlist["playlist_id"],
                )

    def get_tracks_from_playlist(
        self,
        playlist_id: int
    ) -> PlaylistOut:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT track.*
                        FROM track
                        INNER JOIN track_playlist on track_playlist.track_id = track.id
                        INNER JOIN playlist ON track_playlist.playlist_id = playlist.id
                        WHERE playlist.id = %s
                        """,
                    [playlist_id]
                    )
                    rows = cur.fetchall()
                    print("********", rows)
                    tracks = []
                    for row in rows:
                        track = {}
                        for i, column in enumerate(cur.description):
                             track[column.name] = row[i]
                        tracks.append(track)
                    return tracks


            # select track.*
            # from track
            # inner join track_playlist on track.id = track_playlist.track_id
            # where track_playlist.playlist_id = 1
