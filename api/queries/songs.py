import json
from models.songs import SongIn, SongOut
from queries.pool import pool
import requests
from auth_token import get_token
from decouple import Config


SPOTIFY_CLIENT_ID=Config("SPOTIFY_CLIENT_ID")
SPOTIFY_CLIENT_SECRET=Config("SPOTIFY_CLIENT_SECRET")

SPOTIFY_REDIRECT_URI = "https://localhost:8000/callback/"


class SongQueries:
    def search_track_from_spotify(self, search_input=str):
        access_token = get_token()
        result = requests.get(
           f"https://api.spotify.com/v1/search?q={search_input}&type=track",
            headers={"Authorization": f"Bearer {access_token}"},
        )

        content = json.loads(result.content)
        print("+++++++", content)
        return content['tracks']['items']

    def get_track(self, id: int) -> SongOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                    SELECT *
                    FROM track
                    WHERE id = %s;
                    """,
                    [id],
                    )
                    result = None
                    record = cur.fetchone()
                    if record is not None:
                        result = {}
                        for i, column in enumerate(cur.description):
                            result[column.name] = record[i]
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get that song"}

    def add_track(
        self,
        track: SongIn,
    ) -> SongOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO track (spotify_id, name, artist,
                    song_length, album, album_cover)
                    VALUES (%s,%s,%s,%s,%s,%s)
                    RETURNING id;
                    """,
                    [
                        track.spotify_id,
                        track.name,
                        track.artist,
                        track.song_length,
                        track.album,
                        track.album_cover,
                    ],
                )
                id = result.fetchone()[0]
                return SongOut(
                    id=id,
                    spotify_id=track.spotify_id,
                    name=track.name,
                    artist=track.artist,
                    song_length=track.song_length,
                    album=track.album,
                    album_cover=track.album_cover,
                )
